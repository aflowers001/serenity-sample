package uk.co.flatcat.controllers;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "Addition API")
@RequestMapping("/v1")
public class AdditionController {
    private static final Logger logger = LoggerFactory.getLogger(AdditionController.class);

    @PostMapping(path = "/add/{left}/{right}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> executeRoute(@PathVariable("left") int left, @PathVariable("right") int right) {
        return ResponseEntity.ok(left + right);
    }
}
