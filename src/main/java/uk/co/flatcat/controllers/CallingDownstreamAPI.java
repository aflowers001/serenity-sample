package uk.co.flatcat.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CallingDownstreamAPI {
    private String downstreamURL;
    private RestTemplate restTemplate;

    @Autowired
    public CallingDownstreamAPI(@Value("${downstreamurl}") String downstreamURL,  RestTemplateBuilder restTemplateBuilder) {
        this.downstreamURL = downstreamURL;
        this.restTemplate = restTemplateBuilder.build();
    }

    @GetMapping("/api/downstream")
    public ResponseEntity<String> callDownStream() {
        return restTemplate.getForEntity(downstreamURL, String.class);
    }
}
