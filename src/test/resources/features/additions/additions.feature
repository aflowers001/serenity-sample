Feature: As a maths wizard I want to be able to add numbers

  Scenario: A request without numbers should return a 400 error
    Given adding five to ten
    When I make a request to the addition service
    Then I should receive an http code of 400

  Scenario Outline: Valid requests should add numbers together
    Given adding <left> to <right>
    When I make a request to the addition service
    Then I should receive an http code of 200
    And with the answer <answer>
    Examples:
      | left | right | answer |
      | 10   | 20    | 30     |
      | -1   | -5    | -6     |
      | -1   | 5     | 4      |
      | 5    | -3    | 2     |

    