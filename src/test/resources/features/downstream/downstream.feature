Feature: As a caller I want to be able to call through to the downstream API

  Scenario: A get request to the pass through API should call the downstream API
    Given the system is running
    When I make a request to the pass through service
    Then I should call the downstream api
    And I should receive an http code of 200
    And I should receive a response of "hello world"
