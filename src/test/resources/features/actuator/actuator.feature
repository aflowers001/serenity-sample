@monitoring
Feature: As an application monitoring tool I want to check the relevant actuator endpoints to be available

  Scenario Outline: Actuator endpoints should work with no issue
    Given the system is running
    When making a request to the <endpoint> actuator
    Then I should receive an http code of 200
    Examples:
    | endpoint   |
    | health     |
    | prometheus |
    | info       |
    | metrics    |