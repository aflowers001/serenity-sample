package uk.co.flatcat.serenity.stepdefinitions;

import io.cucumber.java.en.Given;
import net.thucydides.core.annotations.Steps;
import uk.co.flatcat.serenity.steps.SpringBootTestStep;

public class InitialiseStepDefinitions {
    @Given("the system is running")
    public void systemIsRunning() {
        // handled by the Spring Boot Test Step library
    }
}
