package uk.co.flatcat.serenity.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import uk.co.flatcat.serenity.steps.SpringBootTestStep;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.CoreMatchers.is;

public class DownstreamStepDefinitions {
    @Steps
    private SpringBootTestStep springBootTestStep;

    @When("I make a request to the pass through service")
    public void makingACallToThePassThroughService() {
        SerenityRest.given()
                .when()
                .get("http://localhost:" + springBootTestStep.getPort() + "/api/downstream");
    }

    @Then("I should call the downstream api")
    public void shouldHaveCalledTheDownstreamAPI() {
        verify(getRequestedFor(urlPathMatching("/api/test")));
    }

    @Then("I should receive a response of {string}")
    public void shouldHaveReceivedTheCorrectHttpStatusCode(String expectedResponse) {
        restAssuredThat(response -> response.body(is(expectedResponse)));
    }
}
