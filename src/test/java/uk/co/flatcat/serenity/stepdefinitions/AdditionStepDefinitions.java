package uk.co.flatcat.serenity.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import uk.co.flatcat.serenity.steps.RequestSteps;

public class AdditionStepDefinitions {
    @Steps
    private RequestSteps requestSteps;

    @Given("^adding (.*) to (.*)$")
    public void adding(String left, String right) {
        requestSteps.setup(left, right);
    }

    @When("^I make a request to the addition service$")
    public void makeRequest() {
        requestSteps.makeRequest();
    }

    @Then("with the answer {word}")
    public void shouldReceiveTheOutput(String expectedResponse) {
        requestSteps.shouldHaveResponseBody(expectedResponse);
    }
}
