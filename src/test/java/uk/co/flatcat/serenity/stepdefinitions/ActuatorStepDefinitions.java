package uk.co.flatcat.serenity.stepdefinitions;

import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import uk.co.flatcat.serenity.steps.SpringBootTestStep;

public class ActuatorStepDefinitions {
    @Steps
    private SpringBootTestStep springBootTestStep;

    @When("making a request to the {word} actuator")
    public void whenMakingARequestToAnActuatorEndpoint(String endpoint) {
        SerenityRest.when().get("http://localhost:" + springBootTestStep.getPort() + "/actuator/" + endpoint);
    }
}
