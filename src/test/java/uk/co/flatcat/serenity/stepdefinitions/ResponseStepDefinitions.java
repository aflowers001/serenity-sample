package uk.co.flatcat.serenity.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import uk.co.flatcat.serenity.steps.SpringBootTestStep;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class ResponseStepDefinitions {
    @Steps
    private SpringBootTestStep springBootTestStep;

    @Then("I should receive an http code of {int}")
    public void shouldHaveReceivedTheCorrectHttpStatusCode(int statusCodeExpected) {
        restAssuredThat(response -> response.statusCode(statusCodeExpected));
    }
}
