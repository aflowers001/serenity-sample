package uk.co.flatcat.serenity.steps;

import com.github.tomakehurst.wiremock.client.WireMock;
import io.cucumber.java.Before;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.ActiveProfiles;
import uk.co.flatcat.SampleApplication;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {SampleApplication.class })
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("test")
public class SpringBootTestStep {
    @LocalServerPort
    private int port;

    public int getPort() {
        return port;
    }

    @Before
    public void initWireMockServer() {

        WireMock.reset();

        stubFor(get(urlPathMatching("/api/test")).willReturn(ok().withBody("hello world")));
    }
}
