package uk.co.flatcat.serenity.steps;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import uk.co.flatcat.serenity.steps.SpringBootTestStep;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class RequestSteps {
    @Steps
    private SpringBootTestStep springBootTestStep;

    private String left = "0";
    private String right = "0";
    private String responseBody;
    private int responseCode;

    public void setup(String left, String right) {
        this.left = left;
        this.right = right;
    }

    public void makeRequest() {
        RequestSpecification request = SerenityRest.given().contentType("application/json").header("Content-Type", "application/json");

        final Response response = request.post("http://localhost:" + springBootTestStep.getPort() + "/v1/add/{left}/{right}", left, right);

        this.responseBody = response.body().asString();
        this.responseCode = response.getStatusCode();
    }

    public void shouldHaveResponseCode(int expectedResponseCode) {
        assertThat(expectedResponseCode, is(responseCode));
    }

    public void shouldHaveResponseBody(String expectedResponse) {
        assertThat(expectedResponse, is(responseBody));
    }
}
