package uk.co.flatcat.serenity;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import net.serenitybdd.junit.spring.integration.SpringIntegrationMethodRule;
import org.junit.Rule;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(plugin = {"pretty"},
        features = "src/test/resources/features/actuator")
public class ActuatorTests {
    @Rule
    public SpringIntegrationMethodRule springIntegration = new SpringIntegrationMethodRule();
}
